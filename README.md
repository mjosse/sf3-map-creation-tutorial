# SF3 Map Creation tutorial

Welcome to this tutorial on how to design maps in Spellforce 3. It applies for all SF3 games.

You can find a fully detailed and illustrated documentation at : https://steamcommunity.com/sharedfiles/filedetails/?id=2914494245

The next section of this file will give you guidelines to not only make maps, but make quality Skirmish PVP maps.

If you have any feedback, please go to the Official Spellforce Discord, and we'll discuss them as we can.

# SF3 Map Creation Guidelines and recommendations

Making a good looking map is nice, but what's even more important is to make a map that feels nice to play, gameplay wise. While RPG maps have a lot of freedom when making the map, RTS maps have certain criteria, easy to quantify, which will really help feeling nice. We'll focus on skirmish maps, but you will see it makes sense for a journey map or a campaign RTS map to have similar features.

The first thing here is that those are **guidelines**. You don't need to follow them all 100%, like you can map a 5 sectors map for example, but be aware of the risk that it will feel unfair. You can have 1 sector with no resource for trolls, and that could be ok. But the more guidelines you avoided the less likely your map will be good.

When you design your map, try to picture how games will play out. If you see it getting stale, if you cannot picture multiple ways of starting/ending the game, then probably the game does not bring variety.

## Map symmetry

This part is stricty reserved to skirmish maps. If you want your map to be played in 1v1, 2v2, 3v3 by players, you need the map to be completely fair. This means
* sector size
* godstone placement to guarantee same walk time from sector to sector
* resource quantities on each sector
* creep types and positions. Even for creep, for example there are 4 types of wyverns, considered hard creeps. However, they still have different power level, and react differently to certain races, same goes for the golems.
* decorations also. This is a bit restrictive when it comes to art, but adding lots of entities on sectors usually means clogging the sectors and restricting the building space. What official maps do to not damage this is by making different ground patterns on each side, this does not impact the visuals ( look at Greykeep gardens minimap for example).

## Map size and shape

The official maps are a good reference for the map size, in terms of border.

However, to make the RTS sector nice, you need 6 to 8 sectors per player, **not counting the main base**. So main base + 6 sectors is the minimum. Being stuck on 5 sectors put trolls in an advantageous position, as they need less sectors to get efficient, and damages orcs, since they will have inherently less barracks to match other races' production.

## Rush distance

This factor is not easy to judge if you are not an experienced player. Take a look at mystic valley for example. You have main base/sector/sector/main base. What it means is that you are actually 1 sector away from the main of the enemy. That kind of configuration encourages rush builds, where you send the hero to the enemy main base and harass workers for 5 mins. This is absolutely not fun, not for the defender, not for the attacker (though a bit more than the defender), and very boring for spectators if any ; and that is because in 2 - 3 mins, you don't have time to develop your economy on the other sectors to not be completely crippled if your main is under attack.

To avoid this, make sure there is a large distance between the 2 main bases.

Also, avoid making it so that the natural creeping route encourages this as well. For example, Ancient city is risky in that regard because if both players take all their sectors in a clockwise manner, after the 6th sector you're both right next to the main and you're encouraged to take a trip to the enemy main base. So you can see, the rush distance is not the distance from main to main, but the distance between the enemy main and your closest natural sector. However, ancient city is fine, in the sense that the defender can completely choose a creeping path that will protect from this. Another **bad** example is Jungle crossroads, where the clockwise symmetry actually makes the left player naturally favored in taking the last iron sector of the bottom player.

## Sector size

It's important to have enough space to build comfortably in your sector. This is a very common problem on some official maps, such as Old Battlegrounds. You have a T3 sector, where you cannot place Aria distilleries. Basically, for the sector to have a nice shape and size, you need to be able to fit 2 Barracks/Stone hall + fill the rest of the workers of each faction.

A good recommended map size is around 8x8 ground patches. This is pretty large to usually allow for 4 barracks, and some more economy buildings, while still having some space for artistic expression. That is 64 patches, so 7x9 works too, 6x(10 or 11) works too. Of course, if you go to 3x21, that doesn't work anymore, it won't feel good. And if you go much larger, then the maps feel huge and you feel like you're walking all the time.

The last thing to take care of is elevation. Elevation makes a map feel very real and realistic, but be careful about elevation and ramps within a sector, they will impede building space.

This sounds complex and restrictive, but if 1 or 2 sectors don't comply here, it's not the end of the world. The player can adapt to what is presented, but if all sectors are tough to use then the map won't be played often.

One way to make sure it looks all right is to make your r16 file import, put down your sectors for 1 player, make the symmetry, calculate the sectors, and then go through all sectors and see if you will be able to place comfortably some stone halls.

This is going to be really hard to adjust if you get it wrong, so pay attention, and don't be afraid to make a map version with no decoration, no style, only the "technical layout" and have it tested by players. This could save you days of editing your map.

## Walkable/Buildable areas

We have already talked about building areas, with sector size and elevation.

Regarding economy buildings, when placing your resource spawns, try to evaluate how many of the buildings you can put next to the resource. If you cannot put any, the resource income will be low and frustrating to play wtih. One **bad** example is the bottom right main base of Fortress stoneguard. The stone has been put in a hole, where only 2 dwarven stone cutters can be put. But dwarves need 3 stones to start with, so necessarily, a dwarf spawning here is disadvantaged. Another bad example is Bitter canyon, where the iron is funnelled into a sort of downward path. So workers need to walk around the path to get to the resource, and then come back. Not cool.

Another thing to think about it walkable areas, and connection between sectors. In the tutorial map, we made a very tiny passage way from player 1 to player 2. This is very bad design. Having chokepoints-like maps generate stale games for skirmishes, while players are looking for dynamic, engaging combat. One very good example of what **not** to do is Mystic valley. Sectors are connected by snake like roads, where 1 lasting AoE spell by a hero blocks the whole army. Keep that in mind while designing your map, open connections are also interesting. One good example is Ancient city, with a very open middle giving access to all the sectors.

You can still give character to open spaces by adding blocking parts. Like a stone, a tree, a house, a pond of deep water. Map features like this gives also something to play around without blocking the map. Of course, keep in mind the symmetry when doing so.

## Resource placement

The first thing about resource placement is wood. Wood is very pretty yes, but random trees everywhere make for terrible and confusing woodcutter placement. One very **bad** example is wasteland of Xu. In most sectors, you will find patches of 10 - 70 wood. So the first 3 4 min, your woodcutter is efficient, and suddently, your workers start walking more than they gather resources, and your build order falls apart.

The main base also has 1 specific requirement for stone. Dwarves start with 3 to 4 stonecutters. Most of the official maps have only 1 stone location. The result is that if the enemy hero goes for a rush, you can completely paralyze the dwarf economy for a long time (and it's quite worth it to trade a few healing potions for delaying the stone even longer). This usually does not happen with wood because there are multiple wood patches in the main (again, avoid having too many, but like 2 big blobs, and a couple very small amounts are fine). The solution to this issue is to make sure you have 2 very separate stone spawns in the main. So 1 enemy hereo can only paralyze half the stone. Then it becomes player micro to bell/unbell the workers at the right time. This makes playing a wood race vs a stone race more fair.

The second thing on resource placement is as we said before allowing for enough space for the building to be next to the resource. Woodcutter usually is not an issue, but Aria distillery can definitely be a problem. Iron mines also, when you put ramps to access stone/iron on another height of the map. You should be able to have 3 stonecutters or 3 iron mines efficient on 1 spot. That also means, having the stone itself accessible by 9 workers.

Scrap is also interesting to design. Gameplay wise, scrap is equivalent to iron, so it would make sense to put it in the same sector as iron. However, since it can be put anywhere on the map, it's fun to place some in a safe space, and some more in a more exposed, but more rewarding space. A **bad** example is Empyrian coast, where you have completely safe scrap, + another 400 a bit harder on the map. Given how fast it gathers, trolls can tech up really fast and produce quality units early on.

## Resource quantities

Given the costs of units and buildings and techs across all races, your maps need to have a good amount of resource to not block the game, but not so much that the resource has no strategic value anymore. Same goes for placement, it's nice to have a mix of safe resource, to not just have a harass game every time with heroes, and some exposed resources to encourage splitting armies and attacking multiple places.

Don't forget the Dark Elves and the Trolls. They don't have classic T3 resource, and trolls don't have iron. So a T3 sector with only T3 resource is really sad for them. Be nice to them, and add some wood or food at least. The bad example of this point is **Windwall forest**, the T3 sectors have nothing to offer to trolls. It's simply farm space, or far barrack space.

T3 resource also needs to be accessible to every player, no matter the configuration. This is another **bad** trait of Wastelands of Xu, because 2 middle sectors have 2 T3 sectors, and the 2 others have the 2 other T3 sectors. Windwall forest also does it wrong, offering 3 T3 sectors for 4 players. If you're unlucky mid game, you won't have a chance to tech up and use your late game to come back. However, the **good** example is Hard rock, it offers a low amount of T3 in the main sector, so that at least you have a window to use some T3 to come back. So for example, Wastelands of Xu could be fine to have that split resources in the middle if there was a little bit (~200) safe T3 resource on one of the 3 main sectors of each player.

In terms of overall quantities, here are a few requirement :
* total > 2000 wood per player
* stone/wood should be between 0.7 and 0.8. Overall, we need less stone, but still dwarves and trolls have a really high need for stone.
* per sector>= 100 food (150 recommended minimum), >=200 food in main. Low food maps give for unique gameplay, but are overall less liked by the community. However, going too high on food means players won't need to bother with farm transition, and that's bad design as well.
* 1000-1500 iron per person. Keep in mind, you need 2 iron ore to make 1 usable iron ingot. So 1000 iron on the map is actually only 500 iron for units. Elves in particular have high requirements in iron, so you want this to be accessible. Make sure on your map to have some safe iron, and some exposed iron. Up to you to choose how you want to design your map.
* scrap/iron should be between 0.33 and 0.5. If you go for low scrap, you can make it safe, but again, don't put too much scrap safe, as top troll players will cheese through the map. Fully neutral scrap is not counted in this, as it can be near impossible to exploit a scrap in a very very exposed area.
* T3 : you want around 800-1000 T3 resource (aria/lenya/moonsilver/blackash), which should be found in all T3 sectors equally (no preference towards aria or something else). Try to make at least 2 sectors with it, to make sure that if the sector is lost, the defending player still has opportunity, but he is on a timer.

To help you controlling that, copy paste the reference file Resources_Per_sector.xslx, and adapt it to your map. This should give you an overview of your plan for resource distribution. Worst case, you'll need to adjust after player feedback, but that's really easy to update.

## Creep quantities

This topic is one where probably there is a lot of different opinions within the community. On the one hand, some heroes are favored before level 5, and some heroes really suffer before they reach level 5. So when you make your creep plan, you are basically deciding whether you are making a low XP map or a high one.

Place your weak creeps to guard the sectors. This design choice that is done in the recent maps is made to help newer players to understand how central heroes are in the game. Usually, 2 - 4 easy creeps are recommended on the map per player. This brings you to either level 2 or 3 (with 4 groups, you are very close to level 4). Currently, there are only 2 types of easy creeps, the zombies and the bandits. Zombies are easier, but bandits give the potion.

Usually, you will see 1 middle level camp. This higher tier camp is usually interesting between min 4 - 6 of the game and stealing a part of it might be the difference that makes 1 player have level 5 and the other stuck on level 4. What you want to do here is to make sure that creep is **earned** and not cheesable with an outpost. A **bad** example is fortress stoneguard, where you pull 2 bears in the outpost, get 1 killed, take the godstone get the other and hop, you're level 5. Medusas, skeleton groups in that regard are good at defending outpost, and the bear should be placed far.

The high/very high tier creeps should also not be on outposts. Again the XP reward should be earned. Another important thing here is that they should be optional. They should not block the path to the enemy, because it will stall the game, and if they are put far enough, going for the creep should be a risk to lose an outpost.

Experiment a bit, but don't be afraid of feedback. Players will know what they want, and again this is easy to fix.

## Godstone

Godstones are pretty convenient for RPG maps. Fast travel for heroes, avoiding lots of walk time. They're nice also in skirmish, however, you want to avoid having too many. If you have too many godstones, you can always teleport wherever you need, so practicing your army movement has no purpose. Usually, 1 godstone in the main, 1 godstone in 1 other sector for each player is enough.

You can also integrate neutral godstones to your map. Neutral godstones are nice to make use of the space on the side of the map, or to make parts of the maps that people would ignore more interesting. However, that's not a requirement for a map to be good, simply some flavour that can be used with moderation

## Merchants

It is paramount to have at least 1 merchant per player. The default always safe design is to have him next to the godstone, so it feels good to take the godstone, buy your equipment, and directly get back into the action. But if you want to put him elsewhere, make sure he appears in a visible area. If your map does not allow it, you can another visibility path and have it give permanent vision for the owner of the main base. Not the best option, but it is an option.

Having only 1 merchant for 2 players however is a problem. The default merchant sells only 1 time each item of each hero. So if you have 2 heroes sharing the merchant, only 1 can use the best equipment. That was an occasional strategry, of players to buy their armor in the enemy sector to force the enemy to come buy to the very far and exposed merchant. It is best to avoid this situation.

Another option for merchants is the rare trader, in Articy. He is the same merchant, but without armors or weapons. Basically, a simple refuel on consumables. This one is ok to be shared by players, it's basically some bonus elements. But again, it's not a required feature of the map, the main base merchant is usually enough.

# Tainted obelisk

The most important thing is to simply not forget about it. It is quite rare that competitive games go long enough to have a titan, but we need the option to unblock the game if it comes to that. The usual way of doing it is to place it in 1 safe sector of each player, often the main base. You can also consider it in 1v1 to be in a middle sector, and then you only have 1 spawn. That gives a timer on the game, but then it can also make defensive races favored if they ever get the sector.

However, 1 for more than 2 players is not enough. For example, Fortress stoneguard, there is a middle sector, which is difficult to take back and it's the only titan spawn. It does give a massive advantage to the team which has the spawn, because 2v2s tend to usually go a bit longer than 1v1s, just because of the dynamics of the game.

## Decoration

In this section, you are quite free to do as you will, as long as you respected most of these requirements above. Keep in mind the symmetry.

For decoration, you can add fake wood, to avoid the wood mini patches mentioned above, as the tutorial showed. If you want both sides to be different, try to look for entities of around the same size. For example, if you have a small bench + well area on one side, you could make it a small altar + statue and incense on the other if you want it different, or a small house or tree.

One thing to take care is how decorations impact the gameplay experience. Again, a **bad** example is fortress stoneguard. The middle towers look nice, but then you cannot see behind them, so the top middle T3 sectors are really hard to work with. It can also be a concern if you do large trees. You could also be tempted to use light effects, but they can impact performances, so use with moderation.

## Checklist

- [ ] My map is symmetrical (Skirmish only)
- [ ] I have at least 6 additional sectors per players
- [ ] I have building space on all sectors, or very few sectors which will be hard to build
- [ ] I avoided chokepoints where i could.
- [ ] I have a good resource distribution and placement, notably I avoided having countless mini wood patches, and I have 2 stone spawns in the main base.
- [ ] I'm close to the recommended values for resources
- [ ] I have some creep groups
- [ ] I made my map pretty without clogging build space
- [ ] I have godstones on the map
- [ ] I have merchants on the maps, at least the main base one
- [ ] I have a titan spawn on the map
- [ ] I have music on my map. No particular requirement, just silent map is not inviting.

Again, these are **guidelines**, if you missed 1 or 2, it's not the end of the world :). But now you know what PvP players are looking for.

