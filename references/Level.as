// ------------------------------------------------------------------------------------------------------------------------------------
// Documentation can be found at: 
// 		http://cp-srv-01/wiki
//		file://///CP-SRV-01/Projects/SF3/LevelDesign/ScriptDokumentation/html/index.html

// includes:
#include "../../basicScripts/EXP1_LevelBase.as"

// ------------------------------------------------------------------------------------------------------------------------------------
// globals:
	


	// todo
	// neutral creep aggresive state rewrk
// ------------------------------------------------------------------------------------------------------------------------------------
// classes:
/*


*/
class Level: EXP1_LevelBase
{
	// -------------------------------------------------------------------------------------------------------------------
	string m_stgVersion = "0.2";
	
	// --------------------------------------------------------------------------------------------------------------------
	// --- constructor --- dont make any changes here
	Level (LevelReference@ _Reference)
	{
		super(_Reference);
	}
	// -------------------------------------------------------------------------------------------------------------------
	// overriden:
	
	// Function that is called when the Level is created for the first time - only called once within the campaign
	void OnCreated () override
	{
		print("[SCRIPT] --- V E R S I O N : LevelBase: "+m_LevelBaseVersion+", and Map Script: " + m_stgVersion+" (Level was just created)");
		
		EXP1_PrepareSkirmishPvP();
		
		// Calling the final functions
		InitEvents(); // register the Events
		InitCommon(); // Start the Level
	}

	// level data had been restored from savegame - ensure version compability here
	void OnLoaded (const uint _uVersion) override
	{
		print("[SCRIPT] --- V E R S I O N : LevelBase: "+m_LevelBaseVersion+", and Map Script: " + m_stgVersion+" (Level was loaded)");
		
		// Call the original OnLoaded
		LevelBase::OnLoaded(_uVersion);
		
		InitCommon();
	}

	// level has been entered
	void OnEntered () override
	{
		print("[SCRIPT] --- V E R S I O N : LevelBase: "+m_LevelBaseVersion+", and Map Script: " + m_stgVersion+" (Level was normally entered)");
		
		// Call the original OnEntered
		LevelBase::OnEntered();
	}
	
	// -------------------------------------------------------------------------------------------------------------------
	// ---- CUSTOM FUNCTIONS AND EVENTS ----------------------------------------------------------------------------------
	/*
		All events are registered in this function. 
		There soudn't be any variable declarations or initialitations 
	*/
	void InitEvents ()
	{

	}
	dictionary m_CreateIDtoSpawnName =
	{
		{"0", ""}
	};

	array<string> NeutralSpawns = { "Spawn_Golem_Neutral"};
	/*
		Is called right at the beginning of the level. Should only be used to start a dialoge, or directly stage an event and so on. 
	*/

	// Collect all spawns for the minimap notifications
	array<string> spawns_easy	= {"Spawn_Zombie_P0", "Spawn_Zombie_P1"};
	array<string> spawns_medium = {"Spawn_Bear_P0", "Spawn_Bear_P1"};								
	array<string> spawns_hard 	= {};
	array<string> spawns_elite 	= {"Spawn_Golem_Neutral"};
	string path_visibility = "SkirmishPlayerVisibility";

	void InitCommon ()
	{
		// Start Dialogue at the beginning
		// or script a small cutscene
		// set unattackable for all tier 01 spawns
		for(uint i = 0; i < NeutralSpawns.length(); i++)
		{
			printNote(""+NeutralSpawns[i]);
			for(uint j = 0; j < m_Reference.GetCreaturesFromSpawn(NeutralSpawns[i]).length(); j++)
			{
				m_Reference.GetCreaturesFromSpawn(NeutralSpawns[i])[j].AllowAttacks(false);
				m_Reference.GetCreaturesFromSpawn(NeutralSpawns[i])[j].SetTargetableByAutoAttacks(false);
				m_CreateIDtoSpawnName.set(""+m_Reference.GetCreaturesFromSpawn(NeutralSpawns[i])[j].GetId(), NeutralSpawns[i]);
				m_Reference.RegisterCreatureEventByIndividuals(Damaged, TOnHeroPartyEvent(@this.Skirmish_OnCreatureDamage), m_Reference.GetCreaturesFromSpawn(NeutralSpawns[i]), true, NeutralSpawns[i]);
				// for debug
			}
		}

		PvP_PrepareCampNotifications(spawns_easy, spawns_medium, spawns_hard,spawns_elite);
		
		array <uint8> arr_PlayerFactions = m_Reference.GetPlayerFactions();
		for(uint i  = 0; i < arr_PlayerFactions.length(); i++)
		{
			m_Reference.AddVisiblePathPartial(arr_PlayerFactions[i], path_visibility);
			m_Reference.RegisterHeroPartyEvent(HeroJoined, TOnHeroPartyExtendedEvent(@this.Skirmish_OnHeroJoinedParty), "HeroParty", arr_PlayerFactions[i]);
		}
	}

	bool Skirmish_OnCreatureDamage(Creature& in _Creature)
	{
		array<string> _allCreeps = m_CreateIDtoSpawnName.getKeys();

		string targetSpawnName = "";
		printNote("targetSpawnName: "+targetSpawnName);
		m_CreateIDtoSpawnName.get(""+_Creature.GetId(), targetSpawnName);

		string tempSpawnName = "";
		for(uint i = 0; i < _allCreeps.length; i++)
		{
			m_CreateIDtoSpawnName.get(""+_allCreeps[i], tempSpawnName);
			printNote("tempSpawnName: "+tempSpawnName);
			if(tempSpawnName == targetSpawnName)
			{
				m_Reference.GetCreatureById(parseInt(_allCreeps[i])).AllowAttacks(true);
				m_Reference.GetCreatureById(parseInt(_allCreeps[i])).SetTargetableByAutoAttacks(true);
			}
		}
		return true;
	}

	bool Skirmish_OnHeroJoinedParty(Creature&in _Creature, Entity[]&in _Params)
	{
		// This is to fill the spellbar with the items
		m_Reference.GetHeroParty(_Creature.GetFaction()).PrepareHeroItem(_Creature, "EXP1_MP_HealthPotion", 9);
		m_Reference.GetHeroParty(_Creature.GetFaction()).PrepareHeroItem(_Creature, "EXP1_MP_FocusPotion", 10);
		m_Reference.GetHeroParty(_Creature.GetFaction()).PrepareHeroTrinketSlot(_Creature, 11);
		_Creature.Equip(m_Reference, Item, "EXP1_TeleportStone", FromInventory, 1);
		return true;
	}
	
	// -------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------
}
