//------------------------------------------------------------------------------------------------------------
//	Map 899
//
//	SkirmishObjective <Objective>						sets the skirmish objective (mode)
//														possible <Objective>s:	Conquest		player loses when all sector centers and units have been killed
//																				DestroyCapital	player loses when the capital has been destroyed
//
//  InitialCamera <"LogicBoxName"> <Faction>			defines a logic box as initial camera for a faction
//
//	Sound
//	{
//		// Target: Either Camera or Avatar, specifies which position must be inside the area in order to apply
//		//		   its settings.
//		// AreaType: Either Zone (for e.g. Logic Boxes or Paths) or Sector (for RTS sectors)
//		// AreaName: Name of the target logic box/logic path/sector within which the settings should be applied
//		<Target> <AreaType> "<AreaName>"
//		{
//			Music	"<Event>"		// music event to play while in this area
//			Ambient	"<Event>"		// ambient event to play while in this area
//			Reverb	"<Event>"		// reverb event to play while in this area
//
//			Parameter	"<ParamName>" <Value>	// parameter value to be set while within this area (multiple allowed)
//
//			SortingOrder	<Value>	// sorting order of zone. if the player is within multiple areas affecting the same
//									// audio track or parameter, the one with the higher sorting order will be used.
//		}
//	}
//
//------------------------------------------------------------------------------------------------------------

SkirmishObjective Conquest
UseGlobalResources true

InitialCamera Camera_Player0 0
InitialCamera Camera_Player1 1